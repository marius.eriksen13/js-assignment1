Heroku computer store app with js, html and css

Introduction

I made this application for an assignment at Noroff School of technology and digital media in Bergen, Norway.
This is the first assingment of the front end module of the Full-Stack java Development course, so the goal with this application is to create a basic html site with javascript, to show our understanding of the basics.

If you want to try the app, it is deployed on heroku:
https://computer-store-assignment-1.herokuapp.com/

If you want to install the project, you can clone it and then run "index.html" in your browser.

Project status
Finished

License
This project is open-source. You are free to use any of the code in your own projects, as long as the work is credited.

Authors
Marius Eriksen
